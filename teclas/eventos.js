var teclas = {
	UP: 38,
	DOWN: 40,
	LEFT: 37,
	RIGHT: 39
};

var cuadrito = document.getElementById("area_de_dibujo");
cuadrito.addEventListener("mousedown", dibujarTeclado);

var papel = cuadrito.getContext("2d");
var x = 100;
var y = 100;

dibujarLinea("red", x-1, y-1, x+1, y+1, papel);

function dibujarTeclado(evento) {
	console.log(evento);
	var colorcito = "green";
	var movimiento = 1;

	dibujarLinea(colorcito, x, y, evento.clientX, evento.clientY, papel);
	x = evento.clientX;
	y = evento.clientY;
/*	switch(evento.keyCode) {
		case teclas.UP:
			dibujarLinea(colorcito, x, y, x, y - movimiento, papel);
			y = y - movimiento;
		break;

		case teclas.DOWN:
			dibujarLinea(colorcito, x, y, x, y + movimiento, papel);
			y = y + movimiento;
		break;

		case teclas.RIGHT:
			dibujarLinea(colorcito, x, y, x + movimiento, y, papel);
			x = x + movimiento;
		break;

		case teclas.LEFT:
			dibujarLinea(colorcito, x, y, x - movimiento, y, papel);
			x = x - movimiento;
		break;

		default:
			console.log("TECLA DESCONOCIDA!");
		break;
	}*/
}

function dibujarLinea(color, x_inicial, y_inicial, x_final, y_final, lienzo) {
	lienzo.beginPath();

	lienzo.strokeStyle = color;
	lienzo.lineWidth = 3;
	lienzo.moveTo(x_inicial, y_inicial);
	lienzo.lineTo(x_final, y_final);
	lienzo.stroke();

	lienzo.closePath();
}