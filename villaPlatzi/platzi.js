// -- variables
var cantidad = aleatorio(1, 8);

var vp = document.getElementById("villaplatzi");
document.addEventListener("keyup", moverCerdo);

var papel = vp.getContext("2d");

var cerdoEspecialX = 250;
var cerdoEspecialY = 250;

// -- objetos
var teclas = {
	UP: 38,
	DOWN: 40,
	LEFT: 37,
	RIGHT: 39
};

var fondo = {
	url: "tile.png",
	cargaOK: false
};

var vaca = {
	url: "vaca.png",
	cargaOK: false
};

var cerdo = {
	url: "cerdo.png",
	cargaOK: false
};

var pollo = {
	url: "pollo.png",
	cargaOK: false
};

// -- atributos
fondo.imagen = new Image();
fondo.imagen.src = fondo.url;
fondo.imagen.addEventListener("load", cargarFondo);

vaca.imagen = new Image();
vaca.imagen.src = vaca.url;
vaca.imagen.addEventListener("load", cargarVacas);

cerdo.imagen = new Image();
cerdo.imagen.src = cerdo.url;
cerdo.imagen.addEventListener("load", cargarCerdos);

pollo.imagen = new Image();
pollo.imagen.src = pollo.url;
pollo.imagen.addEventListener("load", cargarPollos);

// funciones
function cargarFondo() {
	fondo.cargaOK = true;
	dibujar();
}

function cargarVacas() {
	vaca.cargaOK = true;
	dibujar();
}

function cargarCerdos() {
	cerdo.cargaOK = true;
	dibujar();
}

function cargarPollos() {
	pollo.cargaOK = true;
	dibujar();
}

function dibujar() {
	if (fondo.cargaOK) {
		papel.drawImage(fondo.imagen, 0, 0);
	} 
	if (vaca.cargaOK) {
		for(var v=0; v < cantidad; v++) {
			var x = aleatorio(0, 3);
			var y = aleatorio(0, 3);

			x = x * 50;
			y = y * 50;
			papel.drawImage(vaca.imagen, x, y);
		}
	}
	if (cerdo.cargaOK) {
		for(var v=0; v < cantidad; v++) {
			var x = aleatorio(1, 2);
			var y = aleatorio(4, 6);

			x = x * 60;
			y = y * 70;
			papel.drawImage(cerdo.imagen, x, y);
		}
		// seccion de cerdo especial ::
		papel.drawImage(cerdo.imagen, cerdoEspecialX, cerdoEspecialY);
	}
/*	if (pollo.cargaOK) {
		for(var v=0; v < cantidad; v++) {
			var x = aleatorio(0, 3);
			var y = aleatorio(0, 3);

			x = x * 50;
			y = y * 30;

			papel.drawImage(pollo.imagen, x, y);
		}
	}*/
}

function moverCerdo(evento) {
	console.log(evento);
	let movimiento = 5;

	switch(event.keyCode) {
		case teclas.UP:
			if (cerdoEspecialY-movimiento > 0) {
				cerdoEspecialY = cerdoEspecialY - movimiento;
				dibujar();
			}
		break;

		case teclas.DOWN:
			if (cerdoEspecialY+movimiento < 500) {
				cerdoEspecialY = cerdoEspecialY + movimiento;
				dibujar();
			}
		break;

		case teclas.RIGHT:
			if (cerdoEspecialX+movimiento < 500) {
				cerdoEspecialX = cerdoEspecialX + movimiento;
				dibujar();
			}
		break;

		case teclas.LEFT:
			if (cerdoEspecialX-movimiento > 0) {
				cerdoEspecialX = cerdoEspecialX - movimiento;
				dibujar();
			}
		break;

		default:
			console.log("TECLA DESCONOCIDA!");
		break;
	}
}

function aleatorio(min, max) {
	var resultado;
	resultado = Math.floor(Math.random() * (max - min + 1)) + min;
	
	return resultado;
}