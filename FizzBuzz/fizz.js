for (let numero=1; numero <= 100; numero++) {
	if (esDivisiblePor(numero, 3) && esDivisiblePor(numero, 5)) {
		document.write("FIZZ BUZZ!<br>")
	}
	else if (esDivisiblePor(numero, 3)) {
		document.write("FIZZ!<br>");
	} else if (esDivisiblePor(numero, 5)) {
		document.write("BUZZ!<br>");
	} else {
		document.write(numero + "<br>");
	}
}

// FUNCIONES
function esDivisiblePor(numero, divisor) {
	return (numero % divisor == 0);
}