var imagenes = [];
imagenes[50] = "billete50.png";
imagenes[20] = "billete20.png";
imagenes[10] = "billete10.png";

var dineroLiquido = 0;
var dineroEntregado = 0;
var div = 0;
var papeles = 0;

var caja = [];
var entregado = [];

caja.push(new Billete(50, 2));
caja.push(new Billete(20, 5));
caja.push(new Billete(10, 10));

var estado_cajero = document.getElementById("estado_cajero");
var resultado = document.getElementById("resultado");

var b = document.getElementById("extraer");
b.addEventListener("click", entregarDinero);

dineroDisponible();
actualizarDatosDelCajero();

function actualizarDatosDelCajero() {
	estado_cajero.innerHTML = "";
	estado_cajero.innerHTML = "<hr>";

	estado_cajero.innerHTML = "Dinero disponible: " + dineroLiquido + ".<br>";
	for (let billete of caja) {
		estado_cajero.innerHTML += "Billete: $" + billete.valor + " | Cantidad disponible: " + billete.cantidad + ".<br>";
	}
	estado_cajero.innerHTML += "<hr>";
}

function entregarDinero() {
	resultado.innerHTML = "";

	let t = document.getElementById("dinero");
	dineroSolicitado = parseInt(t.value);

	// se podria optimizar para que el usuario NO introduzca valores negativos por pantalla.
	if (dineroSolicitado > 0) {
		if (dineroLiquido >= dineroSolicitado) {
			for (let billete of caja) {
				div = Math.floor(dineroSolicitado / billete.valor);

				papeles = (div > billete.cantidad) ? billete.cantidad : div;

				entregado.push(new Billete(billete.valor, papeles));
				dineroSolicitado = dineroSolicitado - (billete.valor * papeles);

				dineroLiquido = dineroLiquido - (billete.valor * papeles);
				dineroEntregado += billete.valor * papeles;

				actualizarCaja(billete.valor, papeles);
				actualizarDatosDelCajero();
			}

			resultado.innerHTML = "Cantidad entregada: $" + dineroEntregado + ". <br>";
			for(let e of entregado) {
				if (e.cantidad > 0) {
					resultado.innerHTML += e.cantidad + " billetes de $" + e.valor + ".<br>"

					for (let i=0; i < e.cantidad; i++) {
						resultado.innerHTML += "<img src=" + e.imagen.src + ">";
					}
					resultado.innerHTML += "<br>";
				}
			}
			resultado.innerHTML += "<hr>";
			entregado = [];
		} else {
			resultado.innerHTML = "¡No tengo dinero! :(";
		}
	} else {
		resultado.innerHTML = "¡Error! En este momento NO puedo atender este pedido."
	}
}

function actualizarCaja(valorBillete, cantEntregada) {
	for (let billete of caja) {
		if (billete.valor === valorBillete) {
			billete.cantidad -= cantEntregada;
			break;
		}
	}
}

function dineroDisponible() {
	let sumaTotalBilletes = 0;
	for(let billete of caja) {
		sumaTotalBilletes += billete.valor * billete.cantidad;
	}
	this.dineroLiquido = sumaTotalBilletes;
}