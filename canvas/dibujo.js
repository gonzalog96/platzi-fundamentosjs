var texto = document.getElementById("texto_lineas");
var boton = document.getElementById("botoncito");
boton.addEventListener("click", dibujoPorClic);

var d = document.getElementById("dibujito");
var ancho = d.width;
var lienzo = d.getContext("2d");

function dibujarLinea(color, x_inicial, y_inicial, x_final, y_final) {
	lienzo.beginPath();

	lienzo.strokeStyle = color;
	lienzo.moveTo(x_inicial, y_inicial);
	lienzo.lineTo(x_final, y_final);
	lienzo.stroke();

	lienzo.closePath();
}

function dibujoPorClic() {
	var cant_lineas = parseInt(texto.value);
	var yi, xf;
	var colorcito = "#FAA";
	var espacio = ancho / cant_lineas;

	for (l=0; l < cant_lineas; l++) {
		yi = espacio * l;
		xf = espacio * (l + 1);
		

		dibujarLinea("blue", yi, 150, 150, 150-xf);
		dibujarLinea("orange", 300-yi, 150, 150, 150-xf);
		dibujarLinea("red", yi, 150, 150, 150+xf);
		dibujarLinea("green", 300-yi, 150, 150, 150+xf);
	}

	dibujarLinea("black", 150, 0, 150, 300);
	dibujarLinea("black", 0, 150, 300, 150);
	dibujarLinea("green", 0, 0, 0, 300);
	dibujarLinea("green", 0, 300, 300, 300);

	dibujarLinea("green", 0, 0, 300, 0);
	dibujarLinea("green", 300, 0, 300, 300);
}